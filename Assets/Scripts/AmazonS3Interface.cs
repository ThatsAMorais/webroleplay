﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;

public class AmazonS3Interface : MonoBehaviour {

	static readonly string BUCKET = "spare-change";
	static readonly string WEAPONS = "weapons";
	static readonly string ENEMIES = "enemies";

	static readonly string AWS_ACCESS_KEY_ID ="AKIAIBYI4BAKRKT6Z5SQ";
	static readonly string AWS_SECRET_ACCESS_KEY ="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";

	static readonly string AWS_S3_URL_BASE_VIRTUAL_HOSTED = "https://"+BUCKET+".s3.amazonaws.com/";
	static readonly string AWS_S3_URL_BASE_PATH_HOSTED = "https://s3.amazonaws.com/"+BUCKET+"/";
	static readonly string AWS_S3_URL = AWS_S3_URL_BASE_VIRTUAL_HOSTED;


	// Public

	public void requestWeapons(int level)
	{
		SendAmasonS3Request(WEAPONS+"/"+level.ToString() + ".json");
	}

	public void requestEnemies(int level)
	{
		SendAmasonS3Request(ENEMIES+"/"+level.ToString() + ".json");
	}


	// Private

	void SendAmasonS3Request(string itemName)
	{
		Hashtable headers = new Hashtable();

		string dateString =
			System.DateTime.UtcNow.ToString("ddd, dd MMM yyyy HH:mm:ss ") + "GMT";
		headers.Add("x-amz-date", dateString);

		string canonicalString = "GET\n\n\n\nx-amz-date:" + dateString + "\n/" + BUCKET + "/" + itemName;

		// now encode the canonical string
		var ae = new System.Text.UTF8Encoding();
		// create a hashing object
		HMACSHA1 signature = new HMACSHA1();
		// secretId is the hash key
		signature.Key = ae.GetBytes(AWS_SECRET_ACCESS_KEY);
		byte[] bytes  = ae.GetBytes(canonicalString);
		byte[] moreBytes = signature.ComputeHash(bytes);
		// convert the hash byte array into a base64 encoding
		string encodedCanonical = System.Convert.ToBase64String(moreBytes);

		// finally, this is the Authorization header.
		string authorizationString = "AWS " + AWS_ACCESS_KEY_ID + ":" + encodedCanonical;
		headers.Add("Authorization", authorizationString);

		// The URL, either PATH_HOSTED or VIRTUAL_HOSTED including the itemName (a full path like weapons/0
		string url = AWS_S3_URL + itemName;

		// Setup the request url to be sent to Amazon
		WWW www = new WWW(url, null, headers);

		// Send the request in this coroutine so as not to wait busily
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		// Check for errors
		if(www.error == null)
		{
			JSONContentParser.ParseResponse(www.text, www.url, AWS_S3_URL);
		}
		else
		{
			Debug.Log("WWW Error: "+ www.error+" for URL: "+www.url);
		}
	}
}
