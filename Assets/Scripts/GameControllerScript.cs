using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Roll = DiceControllerScript.Roll;
using Player = BattleControllerScript.Player;
using ScoreoidPlayer = ScoreoidInterface.ScoreoidPlayer;
using EnemyData = JSONContentParser.EnemyData;
using WeaponData = JSONContentParser.WeaponData;

public class GameControllerScript : MonoBehaviour {

	// -- Privates
	GameState gameState;
	Player currentCharacter;
	
	bool bWeaponLoadPending = false;
	string pendingWeaponLoadName = "";
	
	public enum RequestState
	{
		UNREQUESTED,
		REQUESTED,
		OBTAINED
	}
	
	Dictionary<int, RequestState> weaponRequestState;
	Dictionary<int, RequestState> enemyRequestState;

					// level 		// Weapon-Type 		// Weapon-Name
	Dictionary<int,Dictionary<string,Weapon>> weapons;
					// level		// Enemy-Name
	Dictionary<int, Dictionary<string, EnemyDefinition>> enemies;
					// level		// Enemy-Name
	Dictionary<int, Dictionary<string, EnemyDefinition>> bosses;
	// -- Unity

	void Awake ()
	{
		gameState = GameState.Title;

		weaponRequestState = new Dictionary<int, RequestState>();
		enemyRequestState = new Dictionary<int, RequestState>();
	}
	
	void Update ()
	{
	}

	void AddWeapon(string theName, Weapon.Type theType,
	               int dmgMod, int spdMod, int defMod,
	               int weaponLevel, Roll weaponRoll,
	               params Attack[] attacks)
	{
		Weapon weapon;

		// Don't create a weapon that doesn't have any attacks
		if(null == attacks || 0 == attacks.Length)
			return;

		weapon = new Weapon(theName, theType, dmgMod, spdMod, defMod, weaponLevel, weaponRoll);
		
		// Add the attacks to the weapon
		foreach(Attack attack in attacks)
		{
			attack.roll = weaponRoll;
			weapon.AddAttack(attack);
		}

		// Ensure the weapons Dict is allocated
		if(null == weapons)
			weapons = new Dictionary<int, Dictionary<string, Weapon>>();

		// Ensure the specified level in 'weapons' is allocated
		if(false == weapons.ContainsKey(weaponLevel))
			weapons[weaponLevel] = new Dictionary<string, Weapon>();

		// Add the weapon
		weapons[weaponLevel].Add(weapon.name, weapon);
	}



	void AddEnemy(string enemyName, Weapon enemyWeapon,
	              float enemyExpValue, float enemyChangeValue,
	              int enemyHealth, int enemySpeed, int enemyDefense,
	              int enemyLevel, bool bBoss=false)
	{
		EnemyDefinition enemy;

		if(null == enemyWeapon)
			return;
		
		enemy = new EnemyDefinition(enemyName, enemyWeapon,
		                            enemyExpValue, enemyChangeValue,
		                            enemyHealth, enemySpeed, enemyDefense,
		                            enemyLevel);

		if(true == bBoss)
		{
			if(null == bosses)
				bosses = new Dictionary<int, Dictionary<string, EnemyDefinition>>();

			if(false == bosses.ContainsKey(enemyLevel))
				bosses[enemyLevel] = new Dictionary<string, EnemyDefinition>();

			bosses[enemyLevel].Add(enemy.name, enemy);
		}
		else
		{
			if(null == enemies)
				enemies = new Dictionary<int, Dictionary<string, EnemyDefinition>>();

			if(false == enemies.ContainsKey(enemyLevel))
				enemies[enemyLevel] = new Dictionary<string, EnemyDefinition>();

			enemies[enemyLevel].Add(enemy.name, enemy);
		}
	}

	void OnEnable ()
	{
	}

	/*
	public List<EnemyDefinition> getEnemiesPerLevel(int level)
	{
		List<EnemyDefinition> enemiesAtLevel = new List<EnemyDefinition>();

		Dictionary<string, EnemyDefinition> temp = new Dictionary<string, EnemyDefinition>();

		// Attempt to get the monsters corresponding to the previous level
		if(enemies.TryGetValue(level-1, out temp))
		{
			// Add these definitions to the return list
			enemiesAtLevel.AddRange(temp.Values);
		}
		// Attempt to get the monsters corresponding to the current level
		if(enemies.TryGetValue(level, out temp))
		{
			// Add these definitions to the return list
			enemiesAtLevel.AddRange(temp.Values);
		}

		return enemiesAtLevel;
	}

	public List<EnemyDefinition> getBossPerLevel(int level)
	{
		List<EnemyDefinition> bosses = new List<EnemyDefinition>();

		Dictionary<string, EnemyDefinition> bossDict = new Dictionary<string, EnemyDefinition>();

		if(true == bosses.TryGetValue(level, out bossDict))
			bosses = new List<EnemyDefinition>(bosses[level].Values);
		else
			bosses = new List<EnemyDefinition>(bosses[0].Values);

		return bosses;
	}

	public EnemyDefinition getEnemyByName(int level, string enemyName)
	{
		EnemyDefinition enemy;

		enemies[level].TryGetValue(enemyName, out enemy);

		return enemy;
	}

	public EnemyDefinition getBossByName(int level, string bossName)
	{
		EnemyDefinition boss;

		bosses[level].TryGetValue(bossName, out boss);
		
		return boss;
	}
	*/


	// -- APIs

	public void setGameState(GameState newGameState)
	{
		gameState = newGameState;
	}

	public GameState getGameState()
	{
		return gameState;
	}

	public Player getCurrentCharacter()
	{
		return currentCharacter;
	}

	public void setCurrentCharacter(ScoreoidPlayer scoreoidPlayer)
	{
		bWeaponLoadPending = false;

		Player player = new Player(scoreoidPlayer.playerName,
									null,
									scoreoidPlayer.xp,
									scoreoidPlayer.change,
									scoreoidPlayer.numberOfKills);

		// Check that this character is up-to-date with the new weapon-loading scheme.  If not, go to weapon-selection
		if((null == scoreoidPlayer.weapon) ||
		   (scoreoidPlayer.weapon.Split("/".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries).Length < 2))
		{
			// Set the game state to weapon selection
			Utilities().setGameState(GameState.WeaponSelection);
			return;
		}

		// May return null if the weapon has not yet been loaded
		player.weapon = getWeapon(scoreoidPlayer.weapon);

		if(null == player.weapon)
		{
			bWeaponLoadPending = true;
			pendingWeaponLoadName = scoreoidPlayer.weapon;
		}

		// Begin retrieving the enemies for the player (and subsequently, those enemies' weapons)
		//Utilities().getEnemies(new List<int>(player.level));
		
		currentCharacter = player;
		Utilities().setGameState(GameState.PlayerProfile);
	}


	public Weapon getWeapon(string levelWeaponName)
	{
		Weapon weapon;
		string[] parsedWeaponString = levelWeaponName.Split("/".ToCharArray(), System.StringSplitOptions.None);
		int level = int.Parse(parsedWeaponString[0]);
		string weaponName = parsedWeaponString[1];

		Dictionary<string, Weapon> weaponLevel = getWeaponLevel(level);
		
		if(null == weaponLevel)
			return null;

		if(false == weaponLevel.TryGetValue(weaponName, out weapon))
			return null;

		return weapon;
	}

	public List<Weapon> getWeapons(int level)
	{
		Dictionary<string, Weapon> weaponLevel = getWeaponLevel(level);

		if(null == weaponLevel)
			return null;

		return new List<Weapon>(weaponLevel.Values);
	}

	Dictionary<string, Weapon> getWeaponLevel(int level)
	{
		Dictionary<string,Weapon> weaponLevel;
		RequestState requestState = RequestState.UNREQUESTED;
		
		if(false == weaponRequestState.TryGetValue(level, out requestState))
			weaponRequestState.Add(level, RequestState.UNREQUESTED);

		switch(requestState)
		{
		case RequestState.UNREQUESTED:
			requestWeaponLevel(level);
			break;
		case RequestState.REQUESTED:
			break;
		case RequestState.OBTAINED:
			// Check that this level is loaded into the weapons cache
			if(weapons.TryGetValue(level, out weaponLevel))
			{
				return weaponLevel;
			}
			break;
		}
		
		return null;
	}

	void requestWeaponLevel(int level)
	{
		if(RequestState.REQUESTED == weaponRequestState[level])
			return;

		weaponRequestState[level] = RequestState.REQUESTED;

		// Pull this level from the server
		Utilities().requestWeapons(level);
	}

	
	public void WeaponLevelRetrieved(int level, List<WeaponData> retrievedWeapons)
	{
		if(null == retrievedWeapons)
		{
			// TODO: track retry count
			// Retry
			requestWeaponLevel(level);
			return;
		}

		weaponRequestState[level] = RequestState.OBTAINED;

				// Key each weapon into the dictionary
		foreach(WeaponData weapon in retrievedWeapons)
		{
			List<Attack> attacks = new List<Attack>();

			foreach(WeaponData.AttackData attack in weapon.attacks)
			{
				attacks.Add(new Attack(attack.name, attack.damage, attack.hit));
			}

			// 
			AddWeapon(weapon.name,
			          ((Weapon.Type) System.Enum.Parse(typeof(Weapon.Type),weapon.type)),
			          weapon.damage, weapon.speed, weapon.defense, weapon.level,
			          new Roll(weapon.rollDie, weapon.rollCount),
			          attacks.ToArray());
		}

		currentCharacter.weapon = getWeapon(pendingWeaponLoadName);

		if(null != enemiesPendingWeaponLoads && 0 < enemiesPendingWeaponLoads.Count)
		{
			List<EnemyDefinition> enemiesWithWeaponsLoaded = new List<EnemyDefinition>();
			foreach(EnemyDefinition enemy in enemiesPendingWeaponLoads.Keys)
			{
				string weaponName = enemiesPendingWeaponLoads[enemy];

				enemy.weapon = getWeapon(weaponName);
				if(null != enemy.weapon)
				{
					enemiesWithWeaponsLoaded.Add(enemy);
				}
			}

			if(0 < enemiesWithWeaponsLoaded.Count)
			{
				// Remove the enemies whose weapons were successfully loaded
				foreach(EnemyDefinition enemyDef in enemiesWithWeaponsLoaded)
				{
					enemiesPendingWeaponLoads.Remove(enemyDef);
				}
			}

			// If all of the enemy weapons have been loaded, handle this accordingly.
			if(0 == enemiesPendingWeaponLoads.Count)
				AllEnemyWeaponsLoaded();
		}
	}



	// Enemies

	List<EnemyDefinition> getEnemyList(List<int> levels, bool bBoss=false)
	{
		List<EnemyDefinition> enemyList = new List<EnemyDefinition>();
		
		foreach(int level in levels)
		{
			Dictionary<string,EnemyDefinition> enemyLevel = getEnemyLevel(level, bBoss);
			
			if(null != enemyLevel)
			{
				enemyList.AddRange(enemyLevel.Values);
			}
		}
		
		return enemyList;
	}

	public List<EnemyDefinition> getEnemies(List<int> levels)
	{
		return getEnemyList(levels);
	}

	public List<EnemyDefinition> getBosses(List<int> levels)
	{
		return getEnemyList(levels, true);
	}

	Dictionary<string,EnemyDefinition> getEnemyLevel(int level, bool bBoss=false)
	{
		Dictionary<string,EnemyDefinition> enemyLevel;
		RequestState requestState = RequestState.UNREQUESTED;
		
		if(false == enemyRequestState.TryGetValue(level, out requestState))
			enemyRequestState.Add(level, RequestState.UNREQUESTED);
		
		switch(requestState)
		{
		case RequestState.UNREQUESTED:
			requestEnemyLevel(level);
			break;
		case RequestState.REQUESTED:
			break;
		case RequestState.OBTAINED:
			if(true == bBoss)
			{
				// Check that this level is loaded into the bosses cache
				if(bosses.TryGetValue(level, out enemyLevel))
				{
					return enemyLevel;
				}
			}
			else
			{
				// Check that this level is loaded into the enemy cache
				if(enemies.TryGetValue(level, out enemyLevel))
				{
					return enemyLevel;
				}
			}
			break;
		}
		
		return null;
	}

	void requestEnemyLevel(int level)
	{
		if(RequestState.REQUESTED == enemyRequestState[level])
			return;
		
		enemyRequestState[level] = RequestState.REQUESTED;
		
		// Pull this level from the server
		Utilities().requestEnemies(level);
	}

	Dictionary<EnemyDefinition, string> enemiesPendingWeaponLoads;

	public void EnemyLevelRetrieved(int level, List<EnemyData> retrievedEnemies)
	{
		if(null == retrievedEnemies)
		{
			// TODO: track retry count
			// Retry
			requestEnemyLevel(level);
			return;
		}
		
		enemyRequestState[level] = RequestState.OBTAINED;
		
		// Check init
		if(null == enemies)
			enemies = new Dictionary<int, Dictionary<string, EnemyDefinition>>();
		if(false == enemies.ContainsKey(level))
			enemies.Add(level, new Dictionary<string, EnemyDefinition>());

		if(null == bosses)
			bosses = new Dictionary<int, Dictionary<string, EnemyDefinition>>();
		if(false == bosses.ContainsKey(level))
			bosses.Add(level, new Dictionary<string, EnemyDefinition>());

		// Separate bosses from enemies
		foreach(EnemyData enemyData in retrievedEnemies)
		{
			EnemyDefinition enemy = new EnemyDefinition(enemyData.name,
			                                            getWeapon(enemyData.weaponName),
			                                            enemyData.xp,
			                                            enemyData.change,
			                                            enemyData.health,
			                                            enemyData.speed,
			                                            enemyData.defense,
			                                            enemyData.level);

			if(null == enemy.weapon)
			{
				if(null == enemiesPendingWeaponLoads)
					enemiesPendingWeaponLoads = new Dictionary<EnemyDefinition, string>();

				enemiesPendingWeaponLoads.Add(enemy, enemyData.weaponName);
			}

			if(true == enemyData.isBoss)
				bosses[level].Add(enemyData.name, enemy);
			else
				enemies[level].Add(enemyData.name, enemy);
		}

		// If all of the enemy weapons have been loaded, handle this accordingly.
		if(null == enemiesPendingWeaponLoads || 0 == enemiesPendingWeaponLoads.Count)
			AllEnemyWeaponsLoaded();
	}

	void AllEnemyWeaponsLoaded()
	{
		Utilities().BattleReady();
	}


	/// <summary>
	/// Game state.
	/// </summary>
	public enum GameState
	{
		NoState,
		Title,
		Register,
		Login,
		Instructions,
		WeaponSelection,
		PlayerProfile,
		BattleMode,
		BattleOver,
	}


	// Description of an Enemy
	public class EnemyDefinition
	{
		public enum Type
		{
			Normal,
			Boss
		}

		public string name {get;set;}
		public Weapon weapon {get;set;}
		public float experienceValue {get;set;}
		public float changeValue {get;set;}
		public int health {get;set;}
		public int speed {get;set;}
		public int defense {get;set;}
		public int level {get;set;}
		public bool isBoss {get;set;}

		public EnemyDefinition(string enemyName, Weapon enemyWeapon,
		                       float enemyExpValue, float enemyChangeValue,
		                       int enemyHealth, int enemySpeed, int enemyDefense, int enemyLevel)
		{
			name = enemyName;
			weapon = enemyWeapon;
			experienceValue = enemyExpValue;
			changeValue = enemyChangeValue;
			health = enemyHealth;
			speed = enemySpeed;
			defense = enemyDefense;
			level = enemyLevel;
		}
	}

	/// <summary>
	/// Weapon.
	/// </summary>
	public class Weapon
	{
		public enum Type
		{
			Melee,
			Ranged,
			Magical,
		}

		public string name {get;set;}
		public Type type {get;set;}
		public int damageModifier {get;set;}
		public int speedModifier {get;set;}
		public int defenseModifier {get;set;}
		public int level {get;set;}
		public Roll roll {get;set;}

		public Dictionary<string,Attack> attacks {get;set;}

		public Weapon(string theName, Type theType,
		              int dmgMod, int spdMod, int defMod, int weaponLevel,
		              Roll weaponRoll)
		{
			name = theName;				// A human-readable name to be displayed in the UI
			type = theType;				// See Weapon.Type
			damageModifier = dmgMod;	// Additional modifiers that the weapon adds to the damage-roll
			speedModifier = spdMod;		// A modifier effecting the weilder's speed
			defenseModifier = defMod;	// A modifier effecting the weilder's defense
			level = weaponLevel;		// the level of this weapon, minimum player level required
			roll = weaponRoll;			// the damage roll

			attacks = new Dictionary<string, Attack>();
		}

		public void AddAttack(Attack attack)
		{
			attacks.Add(attack.name,attack);
		}

		public string getLevelNameString()
		{
			return string.Format("{0}/{1}", level, name);
		}
	}

	public static Roll TO_HIT_ROLL = new Roll("d20",1);

	/// <summary>
	/// Action.
	/// </summary>
	public class BattleAction
	{
		public enum Type
		{
			Attack,
			AttackAll,
			All,
			Self,
			Nothing,
		}

		public string name {get;set;}
		public Roll roll {get;set;}
		public Type type {get;set;}
		public int hitModifier {get;set;}
		
		public BattleAction(Type theType, string theName, int hitMod=0, Roll theRoll=null)
		{
			type = theType;
			name = theName;

			if(null != theRoll)	//  leave it uninitialized, bleh
				roll = theRoll;

			hitModifier = hitMod;
		}
	}
	
	/// <summary>
	/// Attack.
	/// </summary>
	public class Attack : BattleAction
	{
		public int damageModifier {get;set;}

		public Attack(string theName, int dmgMod=0, int hitMod=0,
		              BattleAction.Type actionType=BattleAction.Type.Attack)
			: base((BattleAction.Type.AttackAll == actionType ? actionType : BattleAction.Type.Attack),
			       theName, hitMod)
		{
			damageModifier = dmgMod;
		}
	}


	UtilitiesScript utilitiesScript;
	UtilitiesScript Utilities()
	{
		if(null == utilitiesScript)
			utilitiesScript = GameObject.Find("Utilities").GetComponent<UtilitiesScript>();

		return utilitiesScript;
	}
}
