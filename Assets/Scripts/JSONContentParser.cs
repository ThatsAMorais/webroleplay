﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SimpleJSON;

public class JSONContentParser : MonoBehaviour {

	public class WeaponData
	{
		public class AttackData
		{
			public string name;
			public int damage;
			public int hit;

			public AttackData(string attackName, int attackDmg, int attackHit)
			{
				name = attackName;
				damage = attackDmg;
				hit = attackHit;
			}
		}

		public string name;
		public string type;
		public int damage;
		public int speed;
		public int defense;
		public int level;
		public string rollDie;
		public int rollCount;
		public List<AttackData> attacks;

		public WeaponData(string weaponName, string weaponType, int dmg, int spd, int def, int lvl, string weaponRollDie, int weaponRollCount,
		                  params AttackData[] weaponAttacks)
		{
			name = weaponName;
			type = weaponType;
			damage = dmg;
			speed = spd;
			defense = def;
			level = lvl;
			rollDie = weaponRollDie;
			rollCount = weaponRollCount;
			attacks = new List<AttackData>(weaponAttacks);
		}
	}

	public class EnemyData
	{
		public string name;
		public string weaponName;
		public int xp;
		public int change;
		public int health;
		public int speed;
		public int defense;
		public int level;
		public bool isBoss;
		
		public EnemyData(string enemyName, string enemyWeaponName, int exp, int ch, int hlth, int spd, int def, int lvl, bool bBoss)
		{
			name = enemyName;
			weaponName = enemyWeaponName;
			xp = exp;
			change = ch;
			health = hlth;
			speed = spd;
			defense = def;
			level = lvl;
			isBoss = bBoss;
		}
	}

	/// <summary>
	/// Parses the response.
	/// </summary>
	/// <param name="text">Text.</param>
	/// <param name="url">URL.</param>
	public static void ParseResponse(string text, string url, string baseURL)
	{
		var N = JSON.Parse(text);
	
		UtilitiesScript utilitiesScript = GameObject.Find("Utilities").GetComponent<UtilitiesScript>();

		string errorString = N["error"];
		if(null != errorString)
		{
			Debug.Log(string.Format("Error: {0}", N["error"]));
			utilitiesScript.RequestFailed(N["error"]);
		}
		else
		{
			/* The following will strip the URL of unnecessary components, down to the original item request */
			
			string request = url.Replace(baseURL, "");
			request = request.Replace(".json", "");
			string[] parsedRequest = request.Split("/".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);
			int level = int.Parse(parsedRequest[1]);
			Debug.Log(parsedRequest);
			
			/* The parsed item request is used to determine whether to parse a Weapon or an Enemy */
			
			if(parsedRequest[0].Equals("weapons")) {
				Debug.Log(string.Format("Weapons: {0}", N));
				utilitiesScript.WeaponLevelRetrieved(level, parseWeaponLevelJson(N));
			}
			else if(parsedRequest[0].Equals("enemies")) {
				Debug.Log(string.Format("Enemies: {0}", N));
				utilitiesScript.EnemyLevelRetrieved(level, parseEnemyLevelJson(N));
			}
		}
	}

	/// <summary>
	/// Parses the weapon level json.
	/// </summary>
	/// <returns>list of weapons.</returns>
	/// <param name="weaponLevelData">Weapon level data.</param>
	static List<WeaponData> parseWeaponLevelJson(JSONNode weaponLevelData)
	{
		List<WeaponData> weapons = new List<WeaponData>();
		
		foreach(JSONNode weaponData in weaponLevelData["Weapons"].Childs)
		{
			List<WeaponData.AttackData> attacks = new List<WeaponData.AttackData>();
			foreach(JSONNode attack in weaponData["attacks"].Childs)
			{
				attacks.Add(new WeaponData.AttackData(attack["name"], int.Parse(attack["damage"]), int.Parse(attack["hit"])));
			}

			WeaponData weapon = new WeaponData(weaponData["name"],
			                                   weaponData["type"],
			                                   int.Parse(weaponData["damage"]),
			                                   int.Parse(weaponData["speed"]),
			                                   int.Parse(weaponData["defense"]),
			                                   int.Parse(weaponData["level"]),
			                                   weaponData["roll"]["die"],
			                                   int.Parse(weaponData["roll"]["count"]),
			                                   attacks.ToArray());

			weapons.Add(weapon);
		}
		
		return weapons;
	}

	/// <summary>
	/// Parses the enemy level json.
	/// </summary>
	/// <returns>list of enemies.</returns>
	/// <param name="enemyLevelData">Enemy level data.</param>
	static List<EnemyData> parseEnemyLevelJson(JSONNode enemyLevelData)
	{
		List<EnemyData> enemies = new List<EnemyData>();
		
		foreach(JSONNode enemyData in enemyLevelData["Enemies"].Childs)
		{
			EnemyData enemy = new EnemyData(enemyData["name"],
			                                enemyData["weapon"],
			                                int.Parse(enemyData["XP"]),
			                                int.Parse(enemyData["Change"]),
			                                int.Parse(enemyData["health"]),
			                                int.Parse(enemyData["speed"]),
			                                int.Parse(enemyData["defense"]),
			                                int.Parse(enemyData["level"]),
			                                false);
			enemies.Add(enemy);
		}

		foreach(JSONNode enemyData in enemyLevelData["Bosses"].Childs)
		{
			EnemyData enemy = new EnemyData(enemyData["name"],
			                                enemyData["weapon"],
			                                int.Parse(enemyData["XP"]),
			                                int.Parse(enemyData["Change"]),
			                                int.Parse(enemyData["health"]),
			                                int.Parse(enemyData["speed"]),
			                                int.Parse(enemyData["defense"]),
			                                int.Parse(enemyData["level"]),
			                                true);
			enemies.Add(enemy);
		}

		return enemies;
	}
}