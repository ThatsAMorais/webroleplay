﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ContentStorageInterface : MonoBehaviour {

	// Free, cloud-based storage from Dropbox, and very simple to use
	static readonly string DROPBOX_URL = "dl.dropboxusercontent.com/u/7782207/spare-change/";
	static readonly string WEAPONS = "weapons";
	static readonly string ENEMIES = "enemies";

	static readonly string CONTENT_URL = DROPBOX_URL;


	// Public

	public void requestWeapons(int level)
	{
		SendContentRequest(buildRequest(WEAPONS,level));
	}

	public void requestEnemies(int level)
	{
		SendContentRequest(buildRequest(ENEMIES,level));
	}


	// Private

	string buildRequest(string path, int level)
	{
		return path + "/" + level.ToString() + ".json";
	}

	void SendContentRequest(string request)
	{
		// Setup the request url to be sent to Scoreoid
		WWW www = new WWW(CONTENT_URL + request);
		// Send the request in this coroutine so as not to wait busily
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		// Check for errors
		if(www.error == null)
		{
			JSONContentParser.ParseResponse(www.text, www.url, DROPBOX_URL);
		}
		else
		{
			Debug.Log("WWW Error: "+ www.error+" for URL: "+www.url);
		}
	}
}
